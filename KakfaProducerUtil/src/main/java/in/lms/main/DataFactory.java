package in.lms.main;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class DataFactory {

	private static char COMMA_DELIMITER = ';';
	
	public static DataFactory d = new DataFactory();
	
	public static DataFactory getInstance() {
		return d;
	}
	
	public  Map<Integer, Data> readData(String path) {
		System.out.printf("\n\nLoading %s \n\n",path);
		Map<Integer, Data> dataMap = new HashMap<>();
		try (Reader reader = Files.newBufferedReader(Paths.get(path));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withDelimiter(COMMA_DELIMITER));) {
			for (CSVRecord csvRecord : csvParser) {
				Integer num = Integer.parseInt(csvRecord.get(0));
				Data data = new Data();
				data.setTopicName(csvRecord.get(1));
				data.setMessage(csvRecord.get(2));
				dataMap.put(num, data);
			}
		} catch (IOException e) {
			System.out.println("\n\nError : "+e.getMessage());
		}
		return dataMap;
	}
	
	public  Map<String, String> loadCsv(String path) {
		System.out.printf("\nLoading csv from  %s",path);
		Map<String, String> dataMap = new HashMap<>();
		try (Reader reader = Files.newBufferedReader(Paths.get(path));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withDelimiter(COMMA_DELIMITER));) {
			for (CSVRecord csvRecord : csvParser) {
				dataMap.put(csvRecord.get(0), csvRecord.get(1));
			}
			System.out.println("\nCSV loaded");
		} catch (IOException e) {
			System.out.printf("\nError : %s"+e.getMessage());
		}
		return dataMap;
	}

}
