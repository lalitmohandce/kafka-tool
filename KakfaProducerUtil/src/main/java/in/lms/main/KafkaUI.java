package in.lms.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

/**
 * 
 * @author lamohan
 *
 */
@SuppressWarnings("serial")
public class KafkaUI extends JFrame {

	String headerText = "Kafka Tool - Producer";
	String fontFamily = "Trebuchet MS";

	List<String> topicNames = new ArrayList<>();
	MessageProducer producer = null;

	Map<String, String> dataMap = new HashMap<>();

	JFrame rb;

	JTabbedPane tabbedPane;
	JTextArea messageArea;
	JTextArea logsArea;

	JComboBox<String> topicNameComboBox;

	JTextField filepath;
	JTextField serverUrl;
	JFileChooser fileChooser;

	JLabel fileLabel;
	JLabel serverLabel;
	JLabel topicLabel;
	JLabel messageLabel;

	JButton fileDialog;
	JButton clearLogs;
	JButton ok;
	JButton execute;

	JScrollPane scrollPaneLogArea;
	JScrollPane scrollPaneMessageArea;

	JPanel homePanel;
	JPanel mainPanel;
	JPanel producerPanel;

	JTextPane headPane = new JTextPane();

	Color bgColor = new Color(10, 0, 50);
	Color btnFontColor = Color.BLACK;//new Color(119, 136, 153);
	Color fontColor = Color.WHITE;

	Font btnFont = new Font(fontFamily, Font.PLAIN, 12);
	Font textFont = new Font(fontFamily, Font.PLAIN, 12);

	transient KeyListener kl = new KeyListener() {
		public void keyPressed(KeyEvent ke) {
			if (ke.getKeyCode() == 27) {
				System.exit(0);
			}
		}

		public void keyReleased(KeyEvent ke) {
			System.out.println(ke.getKeyCode());
		}

		public void keyTyped(KeyEvent ke) {
			System.out.println(ke.getKeyCode());
		}
	};

	public class MyComponent extends JComponent {
		int h = 0;
		int w = 0;
		int xpos;
		int ypos;
		String text;

		public MyComponent(int width, int height, String str, int xpos, int ypos) {
			this.h = height;
			this.w = width;
			this.text = str;
			this.xpos = xpos;
			this.ypos = ypos;
		}

		@Override
		public void paint(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;
			Color s = bgColor;
			Color e = Color.WHITE;
			GradientPaint gradient = new GradientPaint(0, 0, s, 400, 400, e, true);
			g2d.setPaint(gradient);
			g2d.drawRect(0, 0, w, h);
			g2d.fillRect(0, 0, w, h);

			Graphics2D g1 = (Graphics2D) g;
			g1.setColor(fontColor);
			int l = 0;
			int margin = 0;
			if (text != null) {
				String[] arr = text.split("\n");
				while (l != arr.length) {
					if (l >= arr.length - 1 && arr[l].contains("Esc")) {
						// g1.setColor(Color.BLACK);
						margin = 22;
					} else if (l >= arr.length - 1) {
						margin = 25;
					} else {
						margin = 20;
					}
					g1.drawString(arr[l], xpos, ypos + (l * margin));
					l++;
				}
			}
		}
	}

	public KafkaUI() {
		setTitle(headerText);
		setSize(660, 520);
		setLocationRelativeTo(null);
		setLayout(null);
		addKeyListener(kl);
		setBackground(Color.WHITE);

		// ---- textPane1 ----
		headPane.setEditable(false);
		headPane.setEnabled(false);
		headPane.setFocusable(false);
		headPane.setFont(new Font(fontFamily, Font.PLAIN, 25));
		headPane.setDisabledTextColor(Color.black);
		headPane.setLayout(new BorderLayout());
		headPane.setBorder(new LineBorder(Color.WHITE, 5));
		headPane.add(new MyComponent(660, 70, headerText, 10, 40));
		headPane.setBounds(0, 0, 660, 70);
		add(headPane);

		JPanel main = new JPanel();
		main.setLayout(new BorderLayout());
		main.setBounds(0, 70, 660, 450);
		main.setBackground(Color.WHITE);
		main.setBorder(new LineBorder(Color.WHITE, 5));
		add(main);

		UIManager.put("CheckBox.background", new Color(255, 255, 255));
		UIManager.put("CheckBox.interiorBackground", Color.black);
		UIManager.put("CheckBox.font", textFont);

		UIManager.put("ScrollBar.thumb", bgColor);
		UIManager.put("ScrollBar.thumbHighlight", bgColor);
		UIManager.put("ScrollBar.thumbShadow", bgColor);
		UIManager.put("ScrollBar.track", Color.WHITE);
		UIManager.put("ScrollPane.background", new Color(255, 255, 255));

		UIManager.put("TabbedPane.selected", new Color(255, 255, 255));
		UIManager.put("TabbedPane.higlight", bgColor);
		UIManager.put("TabbedPane.shadow", bgColor);
		UIManager.put("TabbedPane.focus", new Color(255, 255, 255));

		UIManager.put("SplitPane.background", bgColor);
		UIManager.put("SplitPane.shadow", bgColor);
		UIManager.put("SplitPane.darkShadow", bgColor);
		UIManager.put("SplitPane.highlight", bgColor);

		UIManager.put("Button.background", bgColor);
		UIManager.put("Button.foreground", fontColor);
		UIManager.put("Button.font", textFont);
		UIManager.put("Button.textIconGap", 0);
		UIManager.put("Button.margin", new Insets(0, 0, 0, 0));

		home();
		renderProducerUI();

		tabbedPane = new JTabbedPane();
		tabbedPane.setUI(new BasicTabbedPaneUI());
		tabbedPane.addKeyListener(kl);
		tabbedPane.setFont(btnFont);
		tabbedPane.addTab("Home", homePanel);
		tabbedPane.addTab("Producer", mainPanel);
		tabbedPane.setBackground(bgColor);
		tabbedPane.setForeground(btnFontColor);
		tabbedPane.setBounds(0, 70, 660, 20);
		main.add(tabbedPane);
		setBackground(Color.white);
		setUndecorated(true);
	}

	public void home() {
		homePanel = new JPanel();
		homePanel.setBackground(Color.WHITE);
		homePanel.setLayout(new BorderLayout());
		homePanel.setBorder(new LineBorder(Color.WHITE, 1));
		homePanel.setBounds(0, 70, 660, 410);
		homePanel.addKeyListener(kl);

		JTextPane tP1 = new JTextPane();
		tP1.setEditable(false);
		tP1.setEnabled(false);
		tP1.setFocusable(false);
		tP1.setFont(textFont);
		tP1.setDisabledTextColor(Color.BLACK);
		tP1.setLayout(new BorderLayout());
		tP1.setBorder(new LineBorder(Color.WHITE, 2));
		String text = "Kafka message sender tool for sending messages to different topics."
				+ "\n\nDeveloped by:   Lalit Mohan \nBestseller.\nPress Esc to exit";
		tP1.add(new MyComponent(660, 415, text, 10, 20));
		tP1.setSize(655, 70);
		homePanel.add(tP1);
	}

	public void renderProducerUI() {

		mainPanel = new JPanel();
		mainPanel.setBackground(Color.white);
		mainPanel.setLayout(null);
		mainPanel.setBounds(0, 0, 660, 410);
		mainPanel.addKeyListener(kl);

		producerPanel = new JPanel();

		filepath = new JTextField(30);
		filepath.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
		filepath.setFont(btnFont);
		filepath.setForeground(btnFontColor);

		serverUrl = new JTextField(30);
		serverUrl.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
		serverUrl.setFont(btnFont);
		serverUrl.setForeground(btnFontColor);

		fileChooser = new JFileChooser();
		fileLabel = new JLabel("CSV File");
		fileLabel.setFont(btnFont);
		fileLabel.setForeground(btnFontColor);

		topicLabel = new JLabel("Topic");
		topicLabel.setFont(btnFont);
		topicLabel.setForeground(btnFontColor);

		serverLabel = new JLabel("Kakfa Server");
		serverLabel.setFont(btnFont);
		serverLabel.setForeground(btnFontColor);

		messageLabel = new JLabel("Message");
		messageLabel.setFont(btnFont);
		messageLabel.setForeground(btnFontColor);

		fileDialog = new JButton("Browse");
		fileDialog.setFont(btnFont);
		fileDialog.setForeground(btnFontColor);
		fileDialog.addActionListener(new OpenAction());

		execute = new JButton("Send");
		execute.setFont(btnFont);
		execute.setForeground(btnFontColor);
		execute.addActionListener(new Execute());

		clearLogs = new JButton("Clear Logs");
		clearLogs.setFont(btnFont);
		clearLogs.setForeground(btnFontColor);
		clearLogs.addActionListener(new ClearLogs());

		serverLabel.setBounds(0, 0, 80, 20);
		serverUrl.setBounds(80, 0, 200, 20);

		fileLabel.setBounds(0, 30, 80, 20);
		filepath.setBounds(80, 30, 200, 20);
		fileDialog.setBounds(290, 30, 70, 20);

		topicLabel.setBounds(0, 60, 80, 20);
		topicNameComboBox = new JComboBox<>();
		topicNameComboBox.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
		topicNameComboBox.setVisible(true);
		topicNameComboBox.setBounds(80, 60, 200, 20);
		topicNameComboBox.setBackground(Color.WHITE);
		topicNameComboBox.setFont(btnFont);
		topicNameComboBox.setForeground(btnFontColor);
		topicNameComboBox.addActionListener(new TopicAction());
		execute.setBounds(290, 60, 70, 20);

		messageLabel.setBounds(0, 90, 80, 20);
		messageArea = new JTextArea("");
		messageArea.setVisible(true);
		messageArea.setLineWrap(true);
		messageArea.setFont(btnFont);
		messageArea.setForeground(btnFontColor);
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		scrollPaneMessageArea = new JScrollPane(messageArea);
		scrollPaneMessageArea.setBounds(0, 120, 625, 100);
		scrollPaneMessageArea.setViewportView(messageArea);
		scrollPaneMessageArea.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		messageArea
				.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10)));

		clearLogs.setBounds(290, 90, 70, 20);

		logsArea = new JTextArea("");
		logsArea.setEditable(false);
		logsArea.setVisible(true);
		logsArea.setLineWrap(true);
		logsArea.setFont(btnFont);
		logsArea.setForeground(btnFontColor);

		PrintStream printStream = new PrintStream(new CustomOutputStream(logsArea));
		System.setOut(printStream);

		scrollPaneLogArea = new JScrollPane();
		scrollPaneLogArea.setBounds(0, 230, 625, 160);
		scrollPaneLogArea.setViewportView(logsArea);
		scrollPaneLogArea.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		logsArea.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10)));

		producerPanel.setBackground(Color.white);
		producerPanel.setLayout(null);
		producerPanel.setBounds(10, 10, 660, 410);
		producerPanel.addKeyListener(kl);
		producerPanel.add(fileLabel);
		producerPanel.add(messageLabel);
		producerPanel.add(topicLabel);
		producerPanel.add(serverLabel);
		producerPanel.add(serverUrl);
		producerPanel.add(filepath);
		producerPanel.add(fileDialog);
		producerPanel.add(execute);
		producerPanel.add(clearLogs);
		producerPanel.add(topicNameComboBox);
		producerPanel.add(scrollPaneLogArea);
		producerPanel.add(scrollPaneMessageArea);

		mainPanel.add(producerPanel);

	}

	class OpenAction implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (ae.getSource() == fileDialog) {
				int retval = fileChooser.showOpenDialog(KafkaUI.this);
				if (retval == JFileChooser.APPROVE_OPTION) {
					File f = fileChooser.getSelectedFile();
					filepath.setText(f.getAbsolutePath());
					dataMap = DataFactory.getInstance().loadCsv(f.getAbsolutePath());
					topicNames.addAll(dataMap.keySet());
					topicNameComboBox.setModel(new DefaultComboBoxModel(topicNames.toArray(new String[0])));
				}
			}

			if (ae.getSource() == ok) {
				rb.dispose();
			}
		}

	}

	class Execute implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (!serverUrl.getText().isEmpty()) {
				if (!messageArea.getText().isEmpty()) {
					String message = messageArea.getText();
					if (producer == null) {
						producer = new MessageProducer(serverUrl.getText());
					}
					producer.sendMessage(topicNameComboBox.getSelectedItem().toString(), message);

				} else {
					resultBox("Kafka message can't be null", "Message");
				}

			} else {
				resultBox("Please enter kafka server url", "Message");
			}

			if (ae.getSource() == ok) {
				rb.dispose();
			}
		}

	}

	class ClearLogs implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (ae.getSource() == clearLogs) {
				logsArea.setText("");
			}
		}

	}

	class TopicAction implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			JComboBox combo = (JComboBox) ae.getSource();
			String topic = (String) combo.getSelectedItem();
			String message = dataMap.get(topic);
			messageArea.setText(message);
		}
	}

	public void resultBox(String result, final String title) {
		rb = new JFrame(title);
		rb.setIconImage(new javax.swing.ImageIcon("src/Main/icon.png").getImage());
		rb.setSize(300, 100);
		rb.setLocationRelativeTo(null);
		rb.setUndecorated(true);
		rb.setLayout(new BorderLayout());

		JPanel p1 = new JPanel();
		p1.setBackground(bgColor);
		JLabel l1 = new JLabel(title);
		l1.setFont(textFont);
		l1.setForeground(fontColor);
		p1.add(l1);
		rb.add(p1, BorderLayout.NORTH);
		JLabel l2 = new JLabel(result);
		l2.setFont(textFont);
		JPanel p3 = new JPanel(new FlowLayout());
		p3.add(l2, BorderLayout.CENTER);
		rb.add(p3, BorderLayout.CENTER);

		JPanel p2 = new JPanel(new FlowLayout());
		ok = new JButton("OK");
		ok.setBackground(bgColor);
		ok.setForeground(btnFontColor);
		ok.setActionCommand("ok");
		ok.addActionListener(new OpenAction());
		ok.setSize(20, 30);
		p2.add(ok);
		rb.add(p2, BorderLayout.SOUTH);
		rb.setVisible(true);
	}

	public static void main(String[] args) {
		final KafkaUI mainFrame = new KafkaUI();
		mainFrame.setVisible(true);
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				
			}
		});
	}

}